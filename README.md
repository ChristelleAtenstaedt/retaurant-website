Restaurant/cocktail bar website - CRESCENT

## Description
The website for CRESCENT is divided into the following sections:
    
    A navigation bar.
    A homepage/landing page slideshow with photos of restaurant and drinks.
    An about page with background info about CRESCENT.
    A page with animated menu.
    A contact page with form validation.
    A footer section.
    A customer testimonials section (within the footer). The reviews data is written in a json file which is then rendered dynamically.


## Description
Clone this repository via this link: https://gitlab.nwg-wealth.com/cfg/christelle/restaurant-website.git
   

## Visuals
![Screenshot1](https://gitlab.nwg-wealth.com/cfg/christelle/restaurant-website/-/blob/dea6c3131871c6a64bc77a681d9b9948d44fea67/templates/images/Screenshot1.png)
![Screenshot2](https://gitlab.nwg-wealth.com/cfg/christelle/restaurant-website/-/blob/main/templates/images/Screenshot2.png)


## Technologies
HTML
CSS
JAVASCRIPT
AJAX

## Roadmap
Plans for additional future features:
- A mailing list sign up form
- Slieshow for testimonials




