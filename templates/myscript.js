function getNavBar() {
    return `
    <header>
    <div class="navbar">
     <div class="logo"><a href="home.html">CRESCENT</a></div>
        <ul class="links">
         <li><a href="home.html">HOME</a></li>
         <li><a href="about.html">ABOUT</a></li>
         <li><a href="menu.html">MENU</a></li>
         <li><a href="contact.html">CONTACT</a></li>
     </ul>
        <a href="#" class="action_btn">BOOK A TABLE</a>
         <div class="toggle_btn">
        <i class="fa-solid fa-bars"></i>
    </div>
    </div>

    <div class="dropdown_menu">

     <li><a href="home.html">HOME</a></li>
     <li><a href="about.html">ABOUT</a></li>
     <li><a href="menu.html">MENU</a></li>
     <li><a href="contact.html">CONTACT</a></li>
     <li><a href="#" class="action_btn">BOOK A TABLE</a></li>
    </div>
 </header>

    `;
  }
    
  window.onload = function() {
    let navBarContainer = document.querySelector('div.nav');
    console.log(navBarContainer);
    let footerContainer = document.querySelector('div.footer');
    if (navBarContainer) {
      navBarContainer.innerHTML = getNavBar();
      // togglebutton
const toggleBtn = document.querySelector('.toggle_btn')
const toggleBtnIcon = document.querySelector('.toggle_btn i')
const dropDownMenu = document.querySelector('.dropdown_menu')

toggleBtn.onclick = function () {
    console.log("testing console.log");
    dropDownMenu.classList.toggle('open')
    const isOpen = dropDownMenu.classList.contains('open')

    toggleBtnIcon.classList = isOpen
    ? 'fa-solid fa-xmark'
    : 'fa-solid fa-bars'
}
    }
    if (footerContainer) {
      footerContainer.innerHTML = getFooter();
    }
  }

