// We start our code with an ajax request to fetch the data from the json file

// First I create a new xmlhttp-request object

let http = new XMLHttpRequest();
// the variable http now holds all the methods and properties of the object

// next I will prepare the request with the open() method.

http.open('get', 'customer_reviews.json', true);
// the first argument sets the http method
// in the second argument we pass the file where our data lives
// the last keyword true sets the request to be async.

// Next I will send the request
http.send();

// Now I have to catch the response, I will check the onload eventlistener

http.onload = function(){
    // Inside the function I need to check the readyState and status properties
    if(this.readyState == 4 && this.status == 200) {

        // State 4 means that the request had been sent, the server had finished returning the response and the browser had finished downloading the response content
        // if we have a successful response, I have to parse the json data and convert them to a javascript arrray.
        let customer_reviews = JSON.parse(this.responseText);

        // next I need an empty variable to add the incoming data.
        let output = "";
        // now i have to loop through the customer_reviews and in every iteration i add an html template to the output variable
        for (let item of customer_reviews){
            output+= `
      
    <div class="row">
    <div class="column">
    <div class="card">
    <div class="image">
    <img src="${item.avatar}"> </div>
      <p>${item.name}</p>
      <p>${item.review}</p>
    </div>
  </div>
            
           
            `;
        }



// lastly, I target the products container (div) and add the data that the output variable holds
document.querySelector(".customer_reviews").innerHTML = output;



}
}