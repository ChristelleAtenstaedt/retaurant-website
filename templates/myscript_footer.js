function getFooter() {
    return `
    <body>
    <section class="footer">
    <div class="social">
      <a href="#"><i class="fab fa-instagram"></i></a>
      <a href="#"><i class="fab fa-twitter"></i></a>
      <a href="#"><i class="fab fa-facebook-f"></i></a>
      </div>

      <ul class="list">
        <li>
          <a href="testimonials.html">Testimonials</a>
        </li>
        <li>
        <a href="#">About</a>
        </li>
        <li>
        <a href="#">Terms</a>
        </li>
        <li>
        <a href="#">Privacy Policy</a>
        </li>
    </ul>
    <br>
    <h3 class="copyright">
    CRESCENT@2023</h3>

    </body>

    `;
  }
  
  window.onload = function() {
    let navBarContainer = document.querySelector('div.nav');
    console.log(navBarContainer);
    let footerContainer = document.querySelector('div.footer');
    if (navBarContainer) {
      navBarContainer.innerHTML = getNavBar();
      // togglebutton
const toggleBtn = document.querySelector('.toggle_btn')
const toggleBtnIcon = document.querySelector('.toggle_btn i')
const dropDownMenu = document.querySelector('.dropdown_menu')

toggleBtn.onclick = function () {
    console.log("testing console.log");
    dropDownMenu.classList.toggle('open')
    const isOpen = dropDownMenu.classList.contains('open')

    toggleBtnIcon.classList = isOpen
    ? 'fa-solid fa-xmark'
    : 'fa-solid fa-bars'
}
    }
    if (footerContainer) {
      footerContainer.innerHTML = getFooter();
    }
  }
